<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'blog_category_id', 'id');
        //blogs table ki blog_category_id ka blog_categories table ki id se relation ban jaye ga
        //or phr yeh function humain blog_categories table ka data b sath return kr day ga
        //or view ma hum is function k name se node get kr lain gay 
        //Like <td> {{ $item['category']['blog_category'] }} </td>
    }
}
