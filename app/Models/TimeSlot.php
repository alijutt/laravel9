<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeSlot extends Model
{
    use HasFactory;

    protected $fillable = ['doctor_id', 'start_time', 'end_time', 'day_of_week'];

    public function doctorName()
    {
        return $this->belongsTo(Doctor::class, 'doctor_id', 'id');
    }
}
