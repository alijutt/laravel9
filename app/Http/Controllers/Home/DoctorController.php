<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\TimeSlot;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function AllDoctors()
    {
        $doctors = Doctor::latest()->get();

        return view('admin.doctors.doctor_all', compact('doctors'));
    } // End Method

    public function DoctorDetail()
    {
        $timings = TimeSlot::latest()->get();

        return view('admin.doctors.timingDetail', compact('timings'));
    } // End Method

    public function BookAppointment()
    {
        $timings = TimeSlot::latest()->get();
        $doctors = Doctor::latest()->get();
        return view('admin.doctors.bookAppointment', compact('timings', 'doctors'));
    } // End Method
}
