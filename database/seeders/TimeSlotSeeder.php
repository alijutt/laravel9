<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TimeSlot;

class TimeSlotSeeder extends Seeder
{
    public function run()
    {
        TimeSlot::create([
            'doctor_id' => '1',
            'start_time' => '09:00:00',
            'end_time' => '12:00:00',
            'day_of_week' => 'Monday',
        ]);
    }
}
