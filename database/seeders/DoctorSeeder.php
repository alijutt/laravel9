<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Doctor;

class DoctorSeeder extends Seeder
{
    public function run()
    {
        Doctor::create([
            'name' => 'Dr. John Doe',
            'specialty' => 'Cardiology',
            'status' => 'active'
        ]);
    }
}
