<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('api')->group(function () {
    // Use Route::get() to define a specific route and controller method
    Route::get('/getData', [ApiController::class, 'getData'])->name('api.getData');
});

// Route::controller(ApiController::class)->group(function () {
//     Route::get('/getData', 'getData')->name('api.getData');
// });

//Route::get('getData', 'ApiController@getData');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
